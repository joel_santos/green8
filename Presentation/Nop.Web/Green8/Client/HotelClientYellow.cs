﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Nop.Web.Green8.Models;

namespace Nop.Web.Green8.Client
{
    public class HotelClientYellow
    {
        public static async Task<List<YellowRoom>> GetRooms(DateTime initDate, DateTime endDate)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://192.168.160.27:8080/");

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("WebApplication1/listarQuartosByDate?dateInit=" + initDate.ToString("yyyy-MM-dd") + "&dateEnd=" + endDate.ToString("yyyy-MM-dd"));
                List<YellowRoom> rooms = null;
                if (response.IsSuccessStatusCode)
                {
                    rooms = await response.Content.ReadAsAsync<List<YellowRoom>>();
                    Debug.WriteLine("{0}\t${1}\t{2}", "GetRooms", rooms.Count, "");
                }
                return rooms;
            }
        }
    }
}