﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Nop.Web.Green8.Models;

namespace Nop.Web.Green8.Client
{
    public class HotelClient
    {
        public static async Task<Rooms> GetRooms(DateTime initDate, DateTime endDate)
        {
            var userName = "admin";
            var password = "adminpass";
            var handler = new HttpClientHandler();
            handler.AllowAutoRedirect  = true;
            //handler.Credentials = new System.Net.NetworkCredential(userName, password);

            CredentialCache creds = new CredentialCache();
            creds.Add(new Uri("http://192.168.160.125/"), "basic",
                                    new NetworkCredential(userName, password));
            handler.Credentials = creds;

            using (var client = new HttpClient(handler))
            //using (var client = new HttpClient())
            {
                //DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes(String.Format("{0}:{1}", userName, password))));

                client.BaseAddress = new Uri("http://192.168.160.125/");
                
                //client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes(String.Format("{0}:{1}", userName, password))));

                HttpResponseMessage response = await client.GetAsync("api/room?init_date=" + initDate.ToString("yyyy-MM-dd") + "&end_date=" + endDate.ToString("yyyy-MM-dd") + "&customer=green8");
                Rooms rooms = null;
                if (response.IsSuccessStatusCode)
                {
                    rooms = await response.Content.ReadAsAsync<Rooms>();
                    Debug.WriteLine("{0}\t${1}\t{2}", "GetCars", rooms.rooms.Count, 0);
                }
                return rooms;
            }
        }

        //public static async Task<Rooms> GetRooms(DateTime initDate, DateTime endDate)
        //{
        //    var userName = "admin";
        //    var password = "adminpass";
        //    // Create a request for the URL. 
        //    WebRequest request = WebRequest.Create(
        //      "http://192.168.160.125/api/room?init_date=" + initDate.ToString("yyyy-MM-dd") + "&end_date=" + endDate.ToString("yyyy-MM-dd"));
        //    // If required by the server, set the credentials.
        //    request.Credentials = CredentialCache.DefaultCredentials;
        //    request.Headers["Authorization"] = "Basic YWRtaW46YWRtaW5wYXNz";
        //    // Get the response.
        //    WebResponse response = request.GetResponse();
        //    // Display the status.
        //    Console.WriteLine(((HttpWebResponse)response).StatusDescription);
        //    // Get the stream containing content returned by the server.
        //    Stream dataStream = response.GetResponseStream();
        //    // Open the stream using a StreamReader for easy access.
        //    StreamReader reader = new StreamReader(dataStream);
        //    // Read the content.
        //    string responseFromServer = reader.ReadToEnd();
        //    // Display the content.
        //    Console.WriteLine(responseFromServer);
        //    // Clean up the streams and the response.
        //    reader.Close();
        //    response.Close();
        //}
    }
}