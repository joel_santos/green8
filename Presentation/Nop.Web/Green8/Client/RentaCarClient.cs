﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using Nop.Web.Green8.Models;

namespace Nop.Web.Green8.Client
{
    public class RentaCarClient
    {
        public static async Task<List<Car>> GetCars()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://192.168.160.122:8080/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("opencart/rest/7/car/list");
                List<Car> cars = null;
                if (response.IsSuccessStatusCode)
                {
                    cars = await response.Content.ReadAsAsync<List<Car>>();
                    Debug.WriteLine("{0}\t${1}\t{2}", "GetCars", cars.Count, 0);
                }
                return cars;
            }
        }

        public static async Task<ReservationResult> ReserveCars(List<CarReservation> cars)
        {
            using (var client = new HttpClient())
            {
//                r =
//@"[
//   {
//      'carId':63,
//      'reservationDates': [
//         {
//            'startDate':'20/12/2014',
//            'endDate':'23/12/2014'
//         }
//      ]
//   }
//]";
                client.BaseAddress = new Uri("http://192.168.160.122:8080/");
                //client.BaseAddress = new Uri("http://localhost:15536/");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //var t = JsonConvert.SerializeObject(cars);
                //Debug.WriteLine("{0}\t{1}\t{2}", "ReserveCars r", t, "");
                HttpResponseMessage response = await client.PutAsJsonAsync("opencart/rest/7/order/add", cars);
                //HttpResponseMessage response = await client.PutAsJsonAsync("api/t/testr", cars);
                //HttpResponseMessage response = await client.PutAsJsonAsync("opencart/rest/7/order/add", t);
                //HttpResponseMessage response = await client.PutAsync<String>("opencart/rest/7/order/add", t);
                //HttpResponseMessage response = await client.PutAsync("opencart/rest/7/order/add", t);
                ReservationResult reservationResult = null;
                if (response.IsSuccessStatusCode)
                {
                    //string json = await response.Content.ReadAsStringAsync();
                    //if (json == "INVALID_INPUTS")
                    //{
                    //    Debug.WriteLine("{0}\t{1}\t{2}", "ReserveCars not ok", json, 0);
                    //    return null;
                    //}
                    //reservationResult = JsonConvert.DeserializeObject<ReservationResult>(json);

                    //reservationResult = await response.Content.ReadAsAsync<ReservationResult>();
                    try
                    {
                        reservationResult = await response.Content.ReadAsAsync<ReservationResult>();
                    }
                    catch (Exception)
                    {
                        Debug.WriteLine("{0}\t{1}\t{2}", "ReserveCars not ok", "", "");
                    }
                   
                    Debug.WriteLine("{0}\t{1}\t{2}", "ReserveCars OK", "", "");
                }
                return reservationResult;
            }
        }
    }
}