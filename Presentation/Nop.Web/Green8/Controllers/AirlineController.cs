﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Infrastructure;
using Nop.Services.Authentication;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Web.Green8.Models;
using Nop.Web.Infrastructure.Cache;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.Media;

namespace Nop.Web.Green8.Controllers
{
    [RoutePrefix("api")]
    public class AirlineController : ApiController
    {

        #region Ctor
        public AirlineController()
        {
            _orderProcessingService = EngineContext.Current.Resolve<IOrderProcessingService>();
            _orderService = EngineContext.Current.Resolve<IOrderService>();
            EngineContext.Current.Resolve<IAuthenticationService>();
            _workContext = EngineContext.Current.Resolve<IWorkContext>();
            _storeContext = EngineContext.Current.Resolve<IStoreContext>();
            _productService = EngineContext.Current.Resolve<IProductService>();
            _cacheManager = EngineContext.Current.Resolve<ICacheManager>();
            _specificationAttributeService = EngineContext.Current.Resolve<ISpecificationAttributeService>();
            _localizationService = EngineContext.Current.Resolve<ILocalizationService>();
            _pictureService = EngineContext.Current.Resolve<IPictureService>();
            _orderItemRepository = EngineContext.Current.Resolve<IRepository<OrderItem>>();
            _pdfService = EngineContext.Current.Resolve<IPdfService>();
            _shoppingCartService = EngineContext.Current.Resolve<IShoppingCartService>();
            EngineContext.Current.Resolve<IGenericAttributeService>();
            _productAttributeParser = EngineContext.Current.Resolve<IProductAttributeParser>();
            _customerService = EngineContext.Current.Resolve<ICustomerService>();
        }

        #endregion

        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IOrderService _orderService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IProductService _productService;
        private readonly ICacheManager _cacheManager;
        private readonly ISpecificationAttributeService _specificationAttributeService;
        private readonly IPictureService _pictureService;
        private readonly ILocalizationService _localizationService;
        private readonly IRepository<OrderItem> _orderItemRepository;
        private readonly IPdfService _pdfService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly ICustomerService _customerService;

        /// <summary>
        /// The available airports
        /// </summary>
        /// <returns>A list of all available airports</returns>
        [Route("Airports")]
        [ResponseType(typeof(List<Airport>))]
        public IHttpActionResult GetAirports()
        {                 
            var airports = AirportList();

            if (!airports.Any())
                return NotFound();
           
            return Ok(airports);
        }

        private static List<Airport> AirportList()
        {
            var idOrigem =
                EngineContext.Current.Resolve<IRepository<SpecificationAttribute>>().Table.First(p => p.Name == "Origem").Id;
            var prodRep =
                EngineContext.Current.Resolve<IRepository<SpecificationAttributeOption>>()
                    .Table.Where(p => p.SpecificationAttributeId == idOrigem);
            var airports = prodRep.Select(spec => new Airport {Id = spec.DisplayOrder, Location = spec.Name}).ToList();
            return airports;
        }

        /// <summary>
        /// The available trips
        /// </summary>
        /// <param name="departureDate">Search param, optional.</param>
        /// <param name="airportFrom">Search param, optional.</param>
        /// <param name="airportTo">Search param, optional.</param>
        /// <returns>A list of all available trips</returns>
        [Route("Trips")]
        [ResponseType(typeof(List<Trip>))]
        public IHttpActionResult GetTrips(DateTime? departureDate = null, String airportFrom = null, String airportTo = null)
        {
            var prods = _productService.SearchProducts(categoryIds:new List<int>(){1});

            var specRep = EngineContext.Current.Resolve<IRepository<SpecificationAttributeOption>>();
            if (!prods.Any())
                return NotFound();
            var trips = new List<Trip>();
            var airportsList = AirportList();

            foreach (Product product in prods)
            {
                var trip = new Trip { Id = product.Id, Description = product.Name };
                var pictures = _pictureService.GetPicturesByProductId(product.Id);

                //Imagens
                var defaultPictureModel = new PictureModel()
                {
                    ImageUrl = _pictureService.GetPictureUrl(pictures.FirstOrDefault(), 300),
                    FullSizeImageUrl = _pictureService.GetPictureUrl(pictures.FirstOrDefault()),
                    Title = string.Format(_localizationService.GetResource("Media.Product.ImageLinkTitleFormat.Details"), product.Name),
                    AlternateText = string.Format(_localizationService.GetResource("Media.Product.ImageAlternateTextFormat.Details"), product.Name),
                };
                trip.FullUrl = defaultPictureModel.FullSizeImageUrl;
                trip.ThumbnailUrl = defaultPictureModel.ImageUrl;

                //Specs
                var specificationList = PrepareProductSpecificationModel(product);
                var productSpecificationAttribute = specificationList.FirstOrDefault(p => p.SpecificationAttributeName == "Origem");
                if (productSpecificationAttribute != null)
                    trip.AirportFrom = airportsList.FirstOrDefault(p => p.Location == productSpecificationAttribute.SpecificationAttributeOption);

                productSpecificationAttribute = specificationList.FirstOrDefault(p => p.SpecificationAttributeName == "Destino");
                if (productSpecificationAttribute != null)
                    trip.AirportTo = airportsList.FirstOrDefault(p => p.Location == productSpecificationAttribute.SpecificationAttributeOption);
                TimeSpan durTimeSpan;
                productSpecificationAttribute = specificationList.FirstOrDefault(p => p.SpecificationAttributeName == "Duração");
                if (productSpecificationAttribute != null)
                    if (TimeSpan.TryParse(productSpecificationAttribute.SpecificationAttributeOption, out durTimeSpan))
                        trip.Duration = durTimeSpan;

                productSpecificationAttribute = specificationList.FirstOrDefault(p => p.SpecificationAttributeName == "Hora Partida");

                var seats = HelperGreen8.TripSeats(product);
                if (departureDate != null && !seats.Exists(p => p.DepartureDate == departureDate))//Fazer antes que n sei se dá bodezila por causa da hora
                    continue;

                Product product1 = product;
                var orderItems = _orderItemRepository.Table.Where(p => p.ProductId == product1.Id).ToList();
                var orderAtt = orderItems.Select(d => d.AttributesXml).ToList();

                var reservedSeats = XmlParser.ParseProductVariantAttributeValues(orderAtt);
                foreach (var tripSeat in seats)
                {
                    var alreadyReserved = 0;
                    for (int i = 0; i < reservedSeats.Count; i++)
                    {
                        if (reservedSeats[i].First().Value == tripSeat.DepartureDate)
                            alreadyReserved += orderItems[i].Quantity;
                    }
                    //var alreadyReserved = reservedSeats.Count(p => p.ContainsValue(tripSeat.DepartureDate));
                    tripSeat.AlreadyReservedSeats = alreadyReserved;
                }

                if (seats != null && productSpecificationAttribute != null && TimeSpan.TryParse(productSpecificationAttribute.SpecificationAttributeOption, out durTimeSpan))
                    foreach (var tripSeats in seats)
                        tripSeats.DepartureDate = tripSeats.DepartureDate + durTimeSpan;

                trip.AvailableSeats = seats;

                if (airportFrom != null && trip.AirportFrom != null && airportFrom != trip.AirportFrom.Location)
                    continue;
                if (airportTo != null && trip.AirportTo != null && airportTo != trip.AirportTo.Location)
                    continue;

                trips.Add(trip);
            }
            return Ok(trips);
        }

        /// <summary>
        /// The available trips
        /// </summary>
        /// <param name="tripId">The id of the trip</param>
        /// <returns>Returns a trip with an id</returns>
        [Route("Trips/{tripId:int}")]
        [ResponseType(typeof(Trip))]
        public IHttpActionResult GetTrips(int? tripId = null)
        {
            if (tripId == null) return BadRequest();
            var id = (int)tripId;
            var prods = _productService.GetProductsByIds(new[] { id });

            if (prods.Count != 1) return NotFound();
            if (!prods.Any())
                return NotFound();
            var trips = new List<Trip>();
            var airportsList = AirportList();

            foreach (Product product in prods)
            {
                var trip = new Trip { Id = product.Id, Description = product.Name };
                var pictures = _pictureService.GetPicturesByProductId(product.Id);

                //Imagens
                var defaultPictureModel = new PictureModel()
                {
                    ImageUrl = _pictureService.GetPictureUrl(pictures.FirstOrDefault(), 300),
                    FullSizeImageUrl = _pictureService.GetPictureUrl(pictures.FirstOrDefault()),
                    Title = string.Format(_localizationService.GetResource("Media.Product.ImageLinkTitleFormat.Details"), product.Name),
                    AlternateText = string.Format(_localizationService.GetResource("Media.Product.ImageAlternateTextFormat.Details"), product.Name),
                };
                trip.FullUrl = defaultPictureModel.FullSizeImageUrl;
                trip.ThumbnailUrl = defaultPictureModel.ImageUrl;

                //Specs
                var specificationList = PrepareProductSpecificationModel(product);
                var productSpecificationAttribute = specificationList.FirstOrDefault(p => p.SpecificationAttributeName == "Origem");
                if (productSpecificationAttribute != null)
                    trip.AirportFrom = airportsList.FirstOrDefault(p => p.Location == productSpecificationAttribute.SpecificationAttributeOption);

                productSpecificationAttribute = specificationList.FirstOrDefault(p => p.SpecificationAttributeName == "Destino");
                if (productSpecificationAttribute != null)
                    trip.AirportTo = airportsList.FirstOrDefault(p => p.Location == productSpecificationAttribute.SpecificationAttributeOption);
                TimeSpan durTimeSpan;
                productSpecificationAttribute = specificationList.FirstOrDefault(p => p.SpecificationAttributeName == "Duração");
                if (productSpecificationAttribute != null)
                    if (TimeSpan.TryParse(productSpecificationAttribute.SpecificationAttributeOption, out durTimeSpan))
                        trip.Duration = durTimeSpan;

                productSpecificationAttribute = specificationList.FirstOrDefault(p => p.SpecificationAttributeName == "Hora Partida");

                var seats = HelperGreen8.TripSeats(product);
                Product product1 = product;
                var orderItems = _orderItemRepository.Table.Where(p => p.ProductId == product1.Id).ToList();
                var orderAtt = orderItems.Select(d => d.AttributesXml).ToList();

                var reservedSeats = XmlParser.ParseProductVariantAttributeValues(orderAtt);
                foreach (var tripSeat in seats)
                {
                    var alreadyReserved = 0;
                    for (int i = 0; i < reservedSeats.Count; i++)
                    {
                        if (reservedSeats[i].First().Value == tripSeat.DepartureDate)
                            alreadyReserved += orderItems[i].Quantity;
                    }
                    //var alreadyReserved = reservedSeats.Count(p => p.ContainsValue(tripSeat.DepartureDate));
                    tripSeat.AlreadyReservedSeats = alreadyReserved;
                }

                if (productSpecificationAttribute != null && TimeSpan.TryParse(productSpecificationAttribute.SpecificationAttributeOption, out durTimeSpan))
                    foreach (var tripSeats in seats)
                        tripSeats.DepartureDate = tripSeats.DepartureDate + durTimeSpan;

                trip.AvailableSeats = seats;

                trips.Add(trip);
            }
            return Ok(trips.First());
        }

        /// <summary>
        /// Get invoice by id
        /// </summary>
        /// <param name="orderId">OrderId param, required</param>
        /// <returns>The invoice with that id</returns>
        [Route("Invoices/{orderId:int}")]
        [ResponseType(typeof(Invoice))]
        public IHttpActionResult GetInvoices(int orderId)
        {
            var clienteOrder = _orderService.GetOrderById(orderId);

            if (clienteOrder == null || clienteOrder.Deleted)
                return NotFound();

            var invoice = new Invoice { Id = orderId, Date = clienteOrder.PaidDateUtc == null ? clienteOrder.CreatedOnUtc : clienteOrder.PaidDateUtc.Value, CustomerId = clienteOrder.Customer.Id,CustomerName = clienteOrder.Customer.Username, Price = clienteOrder.OrderTotal, PdfUrl = _pdfService.PrintOrderToPdfWeb(clienteOrder, 0) };


            return Ok(invoice);
        }

        /// <summary>
        /// Gets the invoices for an user
        /// </summary>
        /// <param name="userName">Username, required</param>
        /// <returns>The list of invoice of that Username</returns>
        [Route("Invoices")]
        [ResponseType(typeof(List<Invoice>))]
        public IHttpActionResult GetInvoices(string userName)
        {
            var ordersRep = EngineContext.Current.Resolve<IRepository<Order>>();
            var clienteOrders = ordersRep.Table.Where(p => p.Customer.Username == userName);

            if (!clienteOrders.Any())
                return NotFound();

            return Ok(clienteOrders);
        }




        /// <summary>
        /// Inserts a new reservation
        /// </summary>
        /// <param name="tripId">The Id of the pretended trip</param>
        /// <param name="reservationDate">The date pretended</param>
        /// <param name="numSeats">Number of seats pretended</param>
        /// <param name="firstClass">First Class = true || Coach = false</param>
        /// <param name="userName">Customer Username</param>
        /// <returns></returns>
        [Route("Reservations")]
        [ResponseType(typeof(Invoice))]
        public IHttpActionResult PostReservations(int tripId, string reservationDate, int numSeats, bool firstClass, string userName)
        {

            DateTime data;
            if (!DateTime.TryParse(reservationDate, out data) || numSeats <= 0 || tripId<=0)
                return BadRequest("Invalid Parameters");
            if(DateTime.Compare(data,DateTime.Now)<=0)
                return BadRequest("Date must be at least one day in the future");
            var selectedAtributes = "";
            Product product=null;
            Customer customer = null;
            try
            {
                product = _productService.GetProductById(tripId);

                var classe = firstClass ? "Primeira" : "Económica";

                var attRes = EngineContext.Current.Resolve<IRepository<ProductVariantAttributeValue>>();

                var idAtt = product.ProductVariantAttributes.First(p => p.ProductAttribute.Name == "Classe").Id;

                var productVariantAttributeValue = attRes.Table.FirstOrDefault(p => p.ProductVariantAttributeId == idAtt && p.Name == classe);
                var valAtt = 0;
                if (productVariantAttributeValue != null)
                {
                    valAtt=productVariantAttributeValue.Id;
                }

                selectedAtributes = ParseProductAttributes( product.ProductVariantAttributes, data, valAtt);

                customer = _customerService.GetCustomerByUsername(userName);
            }
            catch (Exception ex)
            {
                return BadRequest("Error: " + ex.Message);
            }

            var warnings = _shoppingCartService.AddToCart(customer, product, (ShoppingCartType)1, _storeContext.CurrentStore.Id, selectedAtributes, decimal.Zero, numSeats, false);
            if (warnings.Count > 0)
            {
                return BadRequest("Error: " + warnings[0]);
            }

           var processPaymentRequest = new ProcessPaymentRequest
           {
               StoreId = _storeContext.CurrentStore.Id,
               CustomerId = customer.Id,
               IsRecurringPayment = false,
               PaymentMethodSystemName = "Payments.CashOnDelivery"
           };

            var placeOrderResult = _orderProcessingService.PlaceOrder(processPaymentRequest);
            if (!placeOrderResult.Success)
            {
                return BadRequest("Error: " + placeOrderResult.Errors.First());
            }

            var order =placeOrderResult.PlacedOrder;

            var invoice = new Invoice { Id = order.Id, Date = order.PaidDateUtc == null ? order.CreatedOnUtc : order.PaidDateUtc.Value, CustomerId = order.Customer.Id, CustomerName = order.Customer.Username, Price = order.OrderTotal, PdfUrl = _pdfService.PrintOrderToPdfWeb(order, 0) };

            return Ok(invoice);
        }

        /// <summary>
        /// Delete a pending reservation
        /// </summary>
        /// <param name="orderId">The order ID</param>
        /// <param name="userName">The Username</param>
        /// <returns></returns>
        [Route("Reservations")]
        [ResponseType(typeof(string))] 
        public IHttpActionResult DeleteReservations(int orderId, string userName)
        {

            var order=_orderService.GetOrderById(orderId);
          
            if (order==null||order.Customer.Username!=userName)
                return BadRequest("Invalid Parameters");
            if (order.OrderStatus!=OrderStatus.Pending||order.Deleted)
                return BadRequest("Only pending orders can be deleted");

            try
            {
               _orderService.DeleteOrder(order);
            }
            catch (Exception ex)
            {
                return BadRequest("Error: "+ex.Message);  
            }
          
            return Ok("Order with the ID "+orderId+" has been deleted!");
        }


        /// <summary>
        /// Updates the status of the reservation to paid
        /// </summary>
        /// <param name="orderId">The order ID</param>
        /// <param name="userName">The Username</param>
        /// <param name="dateOfPayment">Date of the payment</param>
        /// <returns></returns>
        [Route("Reservations")]
        [ResponseType(typeof(string))]
        public IHttpActionResult PutReservations(int orderId, string userName, string dateOfPayment)
        {

            var order = _orderService.GetOrderById(orderId);
            DateTime data;
            if (!DateTime.TryParse(dateOfPayment, out data)||order == null || order.Customer.Username != userName)
                return BadRequest("Invalid Parameters");
            if (order.OrderStatus != OrderStatus.Pending || order.Deleted)
                return BadRequest("Only pending orders can be updated.");
            if(DateTime.Compare(data,DateTime.Now)>0)
                return BadRequest("Date of payment must be less or equal than today.");

            try
            {
                order.OrderStatus = OrderStatus.Complete;
                order.PaidDateUtc = data;
                _orderService.UpdateOrder(order);
            }
            catch (Exception ex)
            {
                return BadRequest("Error: " + ex.Message);
            }

            return Ok("Order with the ID " + orderId + " has been paid! The invoice can be found at " + _pdfService.PrintOrderToPdfWeb(order, 0));
        }


        private IList<ProductSpecificationModel> PrepareProductSpecificationModel(Product product)
        {
            if (product == null)
                throw new ArgumentNullException("product");

            string cacheKey = string.Format(ModelCacheEventConsumer.PRODUCT_SPECS_MODEL_KEY, product.Id, _workContext.WorkingLanguage.Id);
            return _cacheManager.Get(cacheKey, () =>
            {
                var model = _specificationAttributeService.GetProductSpecificationAttributesByProductId(product.Id, null, true)
                   .Select(psa =>
                   {
                       return new ProductSpecificationModel()
                       {
                           SpecificationAttributeId = psa.SpecificationAttributeOption.SpecificationAttributeId,
                           SpecificationAttributeName = psa.SpecificationAttributeOption.SpecificationAttribute.GetLocalized(x => x.Name),
                           SpecificationAttributeOption = !String.IsNullOrEmpty(psa.CustomValue) ? psa.CustomValue : psa.SpecificationAttributeOption.GetLocalized(x => x.Name),
                       };
                   }).ToList();
                return model;
            });
        }


        private string ParseProductAttributes(IEnumerable<ProductVariantAttribute> productVariantAttributes, DateTime dateTime, int valAtt)
        {
            string attributes = "";

            #region Product attributes
            string selectedAttributes = string.Empty;

            foreach (var attribute in productVariantAttributes)
            {
                switch (attribute.AttributeControlType)
                {
                    case AttributeControlType.DropdownList:
                        {
                            if (valAtt > 0)
                                    selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
                                        attribute, valAtt.ToString());
                            
                        }
                        break;
                    case AttributeControlType.Datepicker:
                        {                          
                                selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
                                    attribute, dateTime.ToString("D"));                          
                        }
                        break;
                    default:
                        break;
                }
            }
            attributes = selectedAttributes;

            #endregion

          

            return attributes;
        }

        private readonly IProductAttributeParser _productAttributeParser;

        // GET: api/Books/5
        //[Route("{id:int}")]
        //[ResponseType(typeof(BookDto))]
        //public async Task<IHttpActionResult> GetBook(int id)
        //{
        //    BookDto book = await db.Books.Include(b => b.Author)
        //        .Where(b => b.BookId == id)
        //        .Select(AsBookDto)
        //        .FirstOrDefaultAsync();
        //    if (book == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(book);
        //    //Book book = await db.Books.FindAsync(id);
        //    //if (book == null)
        //    //{
        //    //    return NotFound();
        //    //}

        //    //return Ok(book);
        //}

        //// PUT: api/Books/5
        //[ResponseType(typeof(void))]
        //public async Task<IHttpActionResult> PutBook(int id, Book book)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != book.BookId)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(book).State = EntityState.Modified;

        //    try
        //    {
        //        await db.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!BookExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //// POST: api/Books
        //[ResponseType(typeof(Book))]
        //public async Task<IHttpActionResult> PostBook(Book book)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.Books.Add(book);
        //    await db.SaveChangesAsync();

        //    return CreatedAtRoute("DefaultApi", new { id = book.BookId }, book);
        //}

        //// DELETE: api/Books/5
        //[ResponseType(typeof(Book))]
        //public async Task<IHttpActionResult> DeleteBook(int id)
        //{
        //    Book book = await db.Books.FindAsync(id);
        //    if (book == null)
        //    {
        //        return NotFound();
        //    }

        //    db.Books.Remove(book);
        //    await db.SaveChangesAsync();

        //    return Ok(book);
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        //private bool BookExists(int id)
        //{
        //    return db.Books.Count(e => e.BookId == id) > 0;
        //}

        //[Route("{id:int}/details")]
        //[ResponseType(typeof(BookDetailDto))]
        //public async Task<IHttpActionResult> GetBookDetail(int id)
        //{
        //    var book = await (from b in db.Books.Include(b => b.Author)
        //                      where b.BookId == id
        //                      select new BookDetailDto
        //                      {
        //                          Title = b.Title,
        //                          Genre = b.Genre,
        //                          PublishDate = b.PublishDate,
        //                          Price = b.Price,
        //                          Description = b.Description,
        //                          Author = b.Author.Name
        //                      }).FirstOrDefaultAsync();

        //    if (book == null)
        //    {
        //        return NotFound();
        //    }
        //    return Ok(book);
        //}
    }
}