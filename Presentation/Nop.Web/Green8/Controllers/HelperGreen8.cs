using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Domain.Catalog;
using Nop.Web.Green8.Models;

namespace Nop.Web.Green8.Controllers
{
    public class HelperGreen8
    {

        public static List<TripSeats> TripSeats(Product product)
        {
         var seats = new List<TripSeats>();
            var atributos = product.ProductVariantAttributeCombinations.ToList();
            foreach (var productVariantAttributeCombination in atributos)
            {
                var res = XmlParser.ParseProductVariantAttributeValues(productVariantAttributeCombination.AttributesXml);
                if (res.Count != 1) continue; //something went wrong

                if(DateTime.Compare(res.First().Value,DateTime.Now)<0) continue; //se a data for anterior ao dia de hoje

                if (!seats.Exists(p => p.DepartureDate == res.First().Value))
                    seats.Add(new TripSeats {DepartureDate = res.First().Value, CoachSeats = 0, FirstClassSeats = 0});
                var seat = seats.First(p => p.DepartureDate == res.First().Value);
                if (res.First().Key == "Primeira")
                    seat.FirstClassSeats += productVariantAttributeCombination.StockQuantity;
                else
                    seat.CoachSeats += productVariantAttributeCombination.StockQuantity;
            }
            return seats.OrderBy(p=>p.DepartureDate).ToList();
        }

    }
}