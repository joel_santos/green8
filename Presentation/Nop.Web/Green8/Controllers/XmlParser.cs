using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Infrastructure;

namespace Nop.Web.Green8
{
    public class XmlParser
    {
        public static Dictionary<string, DateTime> ParseProductVariantAttributeValues(string attributes)
        {
            var pvaValues = new Dictionary<string, DateTime>();
            var attRes = EngineContext.Current.Resolve<IRepository<ProductVariantAttributeValue>>();
            List<ProductVariantAttributeValue> listaAtributos = attRes.Table.ToList();

            Dictionary<int, string> pvaValuesStr = ParseValues(attributes);
            DateTime date;
            int pvaValueId;
            List<KeyValuePair<int, string>> vals = pvaValuesStr.ToList();
            if (int.TryParse(vals[0].Value, out pvaValueId) && DateTime.TryParse(vals[1].Value, out date))
            {
                ProductVariantAttributeValue pvaValue = listaAtributos.FirstOrDefault(p => p.Id == pvaValueId);
                if (pvaValue != null)
                {
                    pvaValues.Add(pvaValue.Name, date);
                }
            }
            else if (int.TryParse(vals[1].Value, out pvaValueId) && DateTime.TryParse(vals[0].Value, out date))
            {
                ProductVariantAttributeValue pvaValue = listaAtributos.FirstOrDefault(p => p.Id == pvaValueId);
                if (pvaValue != null)
                {
                    pvaValues.Add(pvaValue.Name, date);
                }
            }
            
            return pvaValues;
        }

        private static Dictionary<int, string> ParseValues(string attributes)
        {
            var selectedProductVariantAttributeValues = new Dictionary<int, string>();
            try
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(attributes);

                XmlNodeList nodeList1 = xmlDoc.SelectNodes(@"//Attributes/ProductVariantAttribute");
                foreach (XmlNode node1 in nodeList1)
                {
                    if (node1.Attributes != null && node1.Attributes["ID"] != null)
                    {
                        string str1 = node1.Attributes["ID"].InnerText.Trim();
                        int id = 0;
                        if (int.TryParse(str1, out id))
                        {                           
                            XmlNodeList nodeList2 = node1.SelectNodes(@"ProductVariantAttributeValue/Value");
                            foreach (XmlNode node2 in nodeList2)
                            {
                                string value = node2.InnerText.Trim();
                                selectedProductVariantAttributeValues.Add(id, value);
                            }
                            
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                Debug.Write(exc.ToString());
            }
            return selectedProductVariantAttributeValues;
        }

        public static List<Dictionary<string,DateTime>> ParseProductVariantAttributeValues(List<string> orderItems)
        {
            var items = new List<Dictionary<string, DateTime>>();
            foreach (var orderItem in orderItems)
            {
                var val=ParseProductVariantAttributeValues(orderItem);
                items.Add(val);
            }
            return items;
        }
    }

}