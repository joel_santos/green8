﻿using System.Globalization;
using Nop.Web.Green8.Client;
using Nop.Web.Green8.Models;
using Nop.Web.Green8.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Nop.Web.Green8.Controllers
{
    [RoutePrefix("api/t")]
    public class TestController : ApiController
    {
        [Route("cars")]
        [ResponseType(typeof(List<Car>))]
        public async Task<IHttpActionResult> GetCreateCrossSell(int ProductId, string dateReservation)
        {
            var cars = await RentaCarClient.GetCars();

            if (!cars.Any())
                return NotFound();

            DateTime data;
            if (!DateTime.TryParse(dateReservation, out data))
                return BadRequest("Invalid parameter dateReservation");

            var lista = new List<int>();
            var newCars = new List<Car>();
            foreach (var car in cars)
            {
                int d = car.MaxDays(data);
                if (d > 0)
                {
                    newCars.Add(car);
                    lista.Add(d);
                }
            }
            var g8 = new Green8Services();
            g8.CreateCrossSell(newCars, ProductId, 3, lista, dateReservation);
            return Ok(newCars);
        }

        [Route("cars")]
        [ResponseType(typeof(List<Car>))]
        public async Task<IHttpActionResult> GetCars()
        {
            var cars = await RentaCarClient.GetCars();

            if (!cars.Any())
                return NotFound();

            var g8 = new Green8Services();
            foreach (var car in cars)
            {
                g8.CreateProduct( car.CarName, (decimal)car.carPrice, car.CarName, car.carPicture,  3);
            }
           

            return Ok(cars);
        }

        [Route("CarsDays")]
        [ResponseType(typeof(Dictionary<int,int>))]
        public async Task<IHttpActionResult> GetCarsDays(string dateReservation)
        {
            DateTime data;
            if (!DateTime.TryParse(dateReservation, out data))
                return BadRequest("Invalid parameter dateReservation");

            var cars = await RentaCarClient.GetCars();

            if (!cars.Any())
                return NotFound();

            var lista = new Dictionary<int, int>();

            foreach (var car in cars)
            {
                int d = car.MaxDays(data);
                if(d>0)
                    lista.Add(car.CarId,d);
            }

            return Ok(lista);
        }

        [Route("carr")]
        [ResponseType(typeof(ReservationResult))]
        public async Task<IHttpActionResult> GetCarReservation()
        {
            var reservations = new List<CarReservation>() { new CarReservation() { carId = 63, reservationDates = new List<TimeIntervalReservation>() { new TimeIntervalReservation() { startDate = "26/12/2015", endDate = "27/12/2015" } } } };
            var reservationResult = await RentaCarClient.ReserveCars(reservations);

            if (reservationResult == null)
                return NotFound();

            return Ok(reservationResult);
        }

        [Route("carr")]
        [ResponseType(typeof(string))]
        public IHttpActionResult PutCarReservation(List<CarReservation> r)
        {
            return Ok("OK");
        }

        [Route("rooms")]
        [ResponseType(typeof(List<YellowRoom>))]
        public async Task<IHttpActionResult> GetRooms()
        {
            var rooms = await HotelClientYellow.GetRooms(DateTime.Now.AddYears(-1), DateTime.Now.AddDays(90));

            if (rooms == null)
                return NotFound();

            if (!rooms.Any())
                return NotFound();

            return Ok(rooms);
        }
    }
}
