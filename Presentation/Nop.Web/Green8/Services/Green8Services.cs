﻿using System.Globalization;
using Nop.Core.Domain.Catalog;
using Nop.Core.Infrastructure;
using Nop.Services.Catalog;
using Nop.Services.Media;
using Nop.Web.Green8.Client;
using Nop.Web.Green8.Models;
using Nop.Web.Models.ShoppingCart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Nop.Web.Green8.Services
{
    public class Green8Services
    {

        private IProductService _productService;
        private ICategoryService _categoryService;
        private readonly IPictureService _pictureService;

        public Green8Services()
        {
            _productService = EngineContext.Current.Resolve<IProductService>();
            _pictureService = EngineContext.Current.Resolve<IPictureService>();
            _categoryService = EngineContext.Current.Resolve<ICategoryService>();
        }

        public static async Task<ReservationResult> PagaCarro(int id, DateTime date, int quant)
        {
            var reservations = new List<CarReservation>() { new CarReservation() { carId = id, reservationDates = new List<TimeIntervalReservation>() { new TimeIntervalReservation() { startDate = date.ToString("dd/MM/yyyy"), endDate =  date.AddDays(quant).ToString("dd/MM/yyyy") } } } };
            var reservationResult = await RentaCarClient.ReserveCars(reservations);
            return reservationResult;
        }

        public void CreateCrossSellProduct(string id,string name, decimal price, string details, string imgUrl, int productId,int category,int quant, string metainfo)
        {
            
            var prod = new Product
            {
                Name = name,
                MetaTitle = id,
                UpdatedOnUtc = DateTime.UtcNow,
                CreatedOnUtc = DateTime.UtcNow,
                MetaDescription = metainfo,
                Price = price,
                FullDescription = details,
                VisibleIndividually = false,
                ProductType = ProductType.SimpleProduct,
                IsShipEnabled = false,
                AvailableStartDateTimeUtc = DateTime.UtcNow,
                Published = true,
                StockQuantity = quant,
                OrderMinimumQuantity = 1,
                OrderMaximumQuantity = quant,
                DisplayStockAvailability = true,
                IsTaxExempt = true,
                ManageInventoryMethod=ManageInventoryMethod.ManageStock
            };
            var cat=_categoryService.GetCategoryById(category);
          
            _productService.InsertProduct(prod);

            var productCategoryCopy = new ProductCategory()
            {
                ProductId = prod.Id,
                CategoryId = cat.Id,
                IsFeaturedProduct = false,
                DisplayOrder = 1
            };

            _categoryService.InsertProductCategory(productCategoryCopy);
           

            if (imgUrl != null) {
                var webClient = new WebClient();
                byte[] imageBytes = webClient.DownloadData(imgUrl);

                var picture = _pictureService.InsertPicture(imageBytes, "image/jpeg", _pictureService.GetPictureSeName(name), true);
                var productPicture = new ProductPicture() { DisplayOrder = 1, Picture = picture, PictureId = picture.Id, Product = prod, ProductId = prod.Id };
                _productService.InsertProductPicture(productPicture);
            }

            var oldProduct = new Product();

            oldProduct = _productService.GetProductById(productId);
            var crossProducts = new CrossSellProduct
            {
                ProductId1 = oldProduct.Id,
                ProductId2 = prod.Id
            };

            _productService.InsertCrossSellProduct(crossProducts);
            _productService.GetCrossSellProductsByProductId1(oldProduct.Id);

            //return Ok(oldProduct.FullDescription);
        }


        public void CreateProduct(string name, decimal price, string details, string imgUrl, int category)
        {

            var prod = new Product
            {
                Name = name,
                UpdatedOnUtc = DateTime.UtcNow,
                CreatedOnUtc = DateTime.UtcNow,
                Price = price,
                FullDescription = details,
                VisibleIndividually = true,
                DisableBuyButton = true,
                ProductType = ProductType.SimpleProduct,
                IsShipEnabled = false,
                AvailableStartDateTimeUtc = DateTime.UtcNow,
                Published = true,
                OrderMinimumQuantity = 1,
                DisplayStockAvailability = true,
                IsTaxExempt = true,
                ManageInventoryMethod = ManageInventoryMethod.ManageStock
            };
            var cat = _categoryService.GetCategoryById(category);

            _productService.InsertProduct(prod);

            var productCategoryCopy = new ProductCategory()
            {
                ProductId = prod.Id,
                CategoryId = cat.Id,
                IsFeaturedProduct = false,
                DisplayOrder = 1
            };

            _categoryService.InsertProductCategory(productCategoryCopy);


            if (imgUrl != null)
            {
                var webClient = new WebClient();
                byte[] imageBytes = webClient.DownloadData(imgUrl);

                var picture = _pictureService.InsertPicture(imageBytes, "image/jpeg", _pictureService.GetPictureSeName(name), true);
                var productPicture = new ProductPicture() { DisplayOrder = 1, Picture = picture, PictureId = picture.Id, Product = prod, ProductId = prod.Id };
                _productService.InsertProductPicture(productPicture);
            }

        }
        //public async void CreateCrossSell(ShoppingCartModel cart, int categoryId, int quantidade)
        //{
        //    var cars = await RentaCarClient.GetCars();

        //    if (cars.Any() && cart.Items.Any())
        //    {
        //        foreach (var carItem in cars)
        //        {
        //            var c = cart.Items.First<Nop.Web.Models.ShoppingCart.ShoppingCartModel.ShoppingCartItemModel>();
        //            CreateCrossSellProduct(carItem.CarName, (decimal) carItem.carPrice, carItem.CarName, carItem.carPicture, c.ProductId,categoryId,quantidade);
        //        }
        //    }
        //}

        public void CreateCrossSell(List<Car> cars, int ProductId,int CategoryId,List<int> Quantity,string data)
        {
            if (cars.Any() && ProductId > 0)
            {
                for (int i=0;i< cars.Count;i++)
                {
                    CreateCrossSellProduct(cars[i].CarId+"",cars[i].CarName, (decimal)cars[i].carPrice, cars[i].CarName, cars[i].carPicture, ProductId, CategoryId, Quantity[i],data);
                }
            }
        }
    }
}