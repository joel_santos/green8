﻿using System;
using System.Collections.Generic;

namespace Nop.Web.Green8.Models
{
    /// <summary>
    /// Trip
    /// </summary>
    public class Trip
    {
        

        /// <summary>
        /// Trip id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Trip Description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Trip Duration
        /// </summary>
        public TimeSpan Duration { get; set; }
        /// <summary>
        /// Trip Origin
        /// </summary>
        public Airport AirportFrom { get; set; }
        /// <summary>
        /// Trip Destiny
        /// </summary>
        public Airport AirportTo { get; set; }
        /// <summary>
        /// Available Seats
        /// </summary>
        public List<TripSeats> AvailableSeats
        {
            get;
            set;
        }
        /// <summary>
        /// Trip Image URL
        /// </summary>
        public string FullUrl { get; set; }
        /// <summary>
        /// Trip Image Thumbnail URL
        /// </summary>
        public string ThumbnailUrl { get; set; }
    }
}