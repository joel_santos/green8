﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FiftyOne.Foundation.Mobile.Detection;

namespace Nop.Web.Green8.Models
{
    public class Car
    {
        public int CarId { get; set; }
        public string CarName { get; set; }
        public string carPicture { get; set; }
        public float carPrice { get; set; }
        public List<TimeInterval> timeInterval { get; set; }

        public int MaxDays(DateTime date)
        {
            int max = 0;
            for (int i=0; i < timeInterval.Count; i++)
            {
                if (date.CompareTo(timeInterval[i].startDate) < 0)
                {
                    max = DaysBetween(date, timeInterval[i].startDate);
                    break;
                }
                if (date.CompareTo(timeInterval[i].startDate)==0)
                {
                    max = 0;
                    break;
                }
                if ((date.CompareTo(timeInterval[i].endDate) > 0)&&(i+1)==timeInterval.Count)
                {
                    max = 7;
                }
            }
            if (max > 7)
                max = 7;
            return max;
        }

        private int DaysBetween(DateTime d1, DateTime d2)
        {
            TimeSpan span = d2.Subtract(d1);
            return (int)span.TotalDays;
        }
    }


    public class CarReservation
    {
        public int carId { get; set; }
        public List<TimeIntervalReservation> reservationDates { get; set; }
    }

    public class TimeInterval
    {
        public DateTime startDate { get;  set; }
        public DateTime endDate { get; set; }
    }

    public class TimeIntervalReservation
    {
        public string startDate { get; set; }
        public string endDate { get; set; }
    }

    public class Reservation
    {
        public int reservationId { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
    }

    public class BillingReserved
    {
        public int carId { get; set; }
        public string carName { get; set; }
        public double carPrice { get; set; }
        public double carPriceNoTax { get; set; }
        public List<Reservation> reservations { get; set; }
    }

    public class ReservationResult
    {
        public int billingId { get; set; }
        public string billingFirstName { get; set; }
        public string billingLastName { get; set; }
        public string billingEmail { get; set; }
        public string billingTelephone { get; set; }
        public string billingAddress { get; set; }
        public string billingSub_Total { get; set; }
        public string billingTotal { get; set; }
        public List<BillingReserved> billingReserved { get; set; }
    }
}