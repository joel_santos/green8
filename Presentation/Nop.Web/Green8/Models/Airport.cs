﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Green8.Models
{
    /// <summary>
    /// Airport
    /// </summary>
    public class Airport
    {
        /// <summary>
        /// Airport id
        /// </summary>
        public int Id { get; set ;}
        /// <summary>
        /// Location name
        /// </summary>
        public string Location { get; set; }
    }
}