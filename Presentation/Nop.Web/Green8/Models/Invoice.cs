﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nop.Core.Domain.Customers;

namespace Nop.Web.Green8.Models
{
    /// <summary>
    /// Invoice
    /// </summary>
    public class Invoice
    {
        /// <summary>
        /// Invoice Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Invoice Date
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Invoice Customer Id
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Invoice Customer Name
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// Invoice price total
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// URL to the PDF
        /// </summary>
        public string PdfUrl { get; set; }
    }
}
