﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Green8.Models
{
    public class YellowRoom
    {
        public string name { get; set; }
        public string image { get; set; }
        public string price { get; set; }
        public string category_id { get; set; }
        public string description { get; set; }
    }
}