﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Green8.Models
{
    /// <summary>
    /// Class stating the available seats for a class and day
    /// </summary>
    public class TripSeats
    {
        /// <summary>
        /// Date of departure
        /// </summary>
        public DateTime DepartureDate { get; set; }

        /// <summary>
        /// Available first class seats
        /// </summary>
        public int FirstClassSeats { get; set; }

        /// <summary>
        /// Available coach seats
        /// </summary>
        public int CoachSeats { get; set; }

        /// <summary>
        /// The number os seats already reserved
        /// </summary>
        public int AlreadyReservedSeats { get; set; }
    }
}