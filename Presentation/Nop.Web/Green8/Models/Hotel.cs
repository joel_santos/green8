﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Green8.Models
{
    public class Feature
    {
        public int id { get; set; }
        public string name { get; set; }
        public int price { get; set; }
    }

    public class Image
    {
        public string imageURL { get; set; }
    }

    public class Room
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int number_of_adult_beds { get; set; }
        public int number_of_child_beds { get; set; }
        public int price { get; set; }
        public List<Feature> features { get; set; }
        public Image image { get; set; }
        public string type { get; set; }
    }

    public class Rooms
    {
        public List<Room> rooms { get; set; }
    }

}